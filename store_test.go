package main

import (
	"reflect"
	"testing"
)

func TestStoreGetAllArgos(t *testing.T) {
	Argos := new()
	t.Run("Return correct number of crew", func(t *testing.T) {
		gotNum := len(Argos.argonauts)
		wantNum := 89
		if gotNum != wantNum {
			t.Errorf("Not the right number of argonauts, got %d, want %d", gotNum, wantNum)
		}
	})
	t.Run("Return correct number of crew", func(t *testing.T) {
		gotNumMeth := len(Argos.getAll())
		wantNumMeth := 89
		if gotNumMeth != wantNumMeth {
			t.Errorf("Not the right number of argonauts in Method, got %d, want %d", gotNumMeth, wantNumMeth)
		}
	})
	t.Run("Return correct argonauts", func(t *testing.T) {
		gotStore := Argos.getAll()
		wantStore := []argonaut{
			{
				ID:   1,
				Name: "Acaste",
				Link: "https://fr.wikipedia.org/wiki/Acaste",
			}, {
				ID:   89,
				Name: "Zétès",
				Link: "https://fr.wikipedia.org/wiki/Bor%C3%A9ades",
			},
		}
		if !reflect.DeepEqual(gotStore[0], wantStore[0]) || !reflect.DeepEqual(gotStore[88], wantStore[1]) {
			t.Errorf("Bad datas, got %#v %#v, want %#v %#v", gotStore[0], gotStore[88], wantStore[0], wantStore[1])
		}
	})
}

func TestGetOneArgonaut(t *testing.T) {
	Argos := new()
	t.Run("Return Héraclès", func(t *testing.T) {
		got, err := Argos.getOne(41)
		want := argonaut{
			ID:   41,
			Name: "Héraclès",
			Link: "https://fr.wikipedia.org/wiki/H%C3%A9racl%C3%A8s",
		}
		if !reflect.DeepEqual(got, want) || err != nil {
			t.Errorf("It is not Héraclès, got %#v want %#v", got, want)

		}
	})
	t.Run("If id missing", func(t *testing.T) {
		got, err := Argos.getOne(0)
		want := argonaut{
			ID:   0,
			Name: "",
			Link: "",
		}
		wanterr := "invalid id"
		if !reflect.DeepEqual(got, want) || err.Error() != wanterr {
			t.Errorf("Handle error doen't work, got %#v %#v want %#v %#v", got, err, want, wanterr)

		}
	})
	t.Run("If id missing", func(t *testing.T) {
		got, err := Argos.getOne(1000)
		want := argonaut{
			ID:   0,
			Name: "",
			Link: "",
		}
		wanterr := "invalid id"
		if !reflect.DeepEqual(got, want) || err.Error() != wanterr {
			t.Errorf("Handle error doen't work, got %#v %#v want %#v %#v", got, err, want, wanterr)

		}
	})
}

func TestDeleteArgonaut(t *testing.T) {
	Argos := new()
	t.Run("Delete Lyncee", func(t *testing.T) {
		err := Argos.delete(60)
		got := len(Argos.getAll())
		want := 88
		if got != want || err != nil {
			t.Errorf("Not the right number of argonauts in Method, got %d, want %d", got, want)
		}
	})
	t.Run("If id missing", func(t *testing.T) {
		got := Argos.delete(0)
		want := "invalid id"
		if got.Error() != want {
			t.Errorf("Handle error doen't work, got %#v want %#v", got, want)
		}
	})
	t.Run("If id missing", func(t *testing.T) {
		got := Argos.delete(1000)
		want := "invalid id"
		if got.Error() != want {
			t.Errorf("Handle error doen't work, got %#v want %#v", got, want)
		}
	})
	t.Run("Delete Admete and check it", func(t *testing.T) {
		Argos.delete(3)
		got, err := Argos.getOne(3)
		want := argonaut{
			ID:   0,
			Name: "",
			Link: "",
		}
		wanterr := "invalid id"
		if !reflect.DeepEqual(got, want) && err.Error() == wanterr {
			t.Errorf("Admete is still here, got %#v %#v want %#v %#v", got, err, want, wanterr)

		}
	})
}

func TestUpdateArgonaut(t *testing.T) {
	Argos := new()
	t.Run("It is Hercule not Héraclès", func(t *testing.T) {
		got, err := Argos.update(41, "Hercule", "https://fr.wikipedia.org/wiki/Hercule")
		want := argonaut{
			ID:   41,
			Name: "Hercule",
			Link: "https://fr.wikipedia.org/wiki/Hercule",
		}
		if !reflect.DeepEqual(got, want) || err != nil {
			t.Errorf("It is not Hercule, got %#v want %#v", got, want)

		}
	})
	t.Run("If id missing", func(t *testing.T) {
		got, err := Argos.update(0, "", "")
		want := argonaut{
			ID:   0,
			Name: "",
			Link: "",
		}
		wanterr := "invalid id"
		if !reflect.DeepEqual(got, want) || err.Error() != wanterr {
			t.Errorf("Handle error doen't work, got %#v %#v want %#v %#v", got, err, want, wanterr)

		}
	})
	t.Run("If id missing", func(t *testing.T) {
		got, err := Argos.update(1000, "", "")
		want := argonaut{
			ID:   0,
			Name: "",
			Link: "",
		}
		wanterr := "invalid id"
		if !reflect.DeepEqual(got, want) || err.Error() != wanterr {
			t.Errorf("Handle error doen't work, got %#v %#v want %#v %#v", got, err, want, wanterr)

		}
	})
}

func TestCreateArgonaut(t *testing.T) {
	Argos := new()
	t.Run("Add members to the crew", func(t *testing.T) {
		Argos.create("???", "https://en.wikipedia.org/wiki/???")
		Argos.create("!!!", "https://en.wikipedia.org/wiki/!!!")
		gotNum := len(Argos.getAll())
		wantNum := 91
		if gotNum != wantNum {
			t.Errorf("Not the right number of argonauts, got %d, want %d", gotNum, wantNum)
		}
	})
	t.Run("Add Medea to the crew", func(t *testing.T) {
		ret := Argos.create("Medea", "https://en.wikipedia.org/wiki/Medea")
		got, _ := Argos.getOne(92)
		want := argonaut{
			ID:   92,
			Name: "Medea",
			Link: "https://en.wikipedia.org/wiki/Medea",
		}
		if !reflect.DeepEqual(got, want) {
			t.Errorf("Medea is not in the boat ! got %#v, ret %#v, want %#v", got, ret, want)

		}
	})
}

func TestLotofChanges(t *testing.T) {
	Argos := new()
	t.Run("Delete five and add two then count", func(t *testing.T) {
		Argos.delete(1)
		Argos.delete(2)
		Argos.delete(3)
		Argos.delete(4)
		Argos.delete(5)
		Argos.create("???", "https://en.wikipedia.org/wiki/???")
		Argos.create("!!!", "https://en.wikipedia.org/wiki/!!!")

		gotNum := len(Argos.getAll())
		wantNum := 89 - 5 + 2
		if gotNum != wantNum {
			t.Errorf("Not the right number of argonauts, got %d, want %d", gotNum, wantNum)
		}
	})
}
