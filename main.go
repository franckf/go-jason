package main

import (
	"context"
	_ "embed"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

//go:embed argonauts.json
var argonauts string

type argonaut struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
	Link string `json:"link"`
}

type store struct {
	argonauts []argonaut
}

type inputs struct {
	Name string `json:"name"`
	Link string `json:"link"`
}

func new() (newCrew *store) {
	newCrew = &store{}
	err := json.Unmarshal([]byte(argonauts), &newCrew.argonauts)
	if err != nil {
		print("ERROR: unmarshall argonauts", err)
	}
	return
}

func (s *store) getAll() []argonaut {
	return s.argonauts
}

func (s *store) getOne(id int) (crewMember argonaut, err error) {
	for index, argonaut := range s.argonauts {
		if argonaut.ID == id {
			crewMember = s.argonauts[index]
			return
		}
	}
	err = fmt.Errorf("invalid id")
	return
}

func (s *store) delete(id int) (err error) {
	for index, argonaut := range s.argonauts {
		if argonaut.ID == id {
			s.argonauts = append(s.argonauts[:index], s.argonauts[index+1:]...)
			return
		}
	}
	err = fmt.Errorf("invalid id")
	return
}

func (s *store) update(id int, name, link string) (crewMember argonaut, err error) {
	for index, argonaut := range s.argonauts {
		if argonaut.ID == id {
			s.argonauts[index].Link = link
			s.argonauts[index].Name = name
			crewMember = s.argonauts[index]
			return
		}
	}
	err = fmt.Errorf("invalid id")
	return
}

func (s *store) create(name, link string) (crewMember argonaut) {
	var newID int
	for _, argonaut := range s.argonauts {
		if argonaut.ID >= newID {
			newID++
		}
	}
	crewMember.ID = newID + 1
	crewMember.Name = name
	crewMember.Link = link
	s.argonauts = append(s.argonauts, crewMember)
	return
}

//go:embed movie.json
var movie string

type movieS struct {
	Title              string   `json:"title"`
	Director           string   `json:"director"`
	Screenplay         []string `json:"screenplay"`
	From               string   `json:"from"`
	Produced           string   `json:"produced"`
	Starring           []string `json:"starring"`
	Cinematography     string   `json:"cinematography"`
	Edited             string   `json:"edited"`
	Music              string   `json:"music"`
	ProductionCompagny string   `json:"production company"`
	Distributed        string   `json:"distributed"`
	ReleaseDate        string   `json:"release date"`
	ReleaseYear        int      `json:"release year"`
	Duration           int      `json:"duration"`
	Countries          []string `json:"countries"`
	Language           string   `json:"language"`
	Budget             string   `json:"budget"`
	BoxOffice          string   `json:"box office"`
	Gender             []string `json:"gender"`
	Map                string   `json:"map"`
}

// Server use datas from store to provide server,
// name is not great
type Server struct {
	argonautsStore *store
}

func newServer() *Server {
	store := new()
	return &Server{argonautsStore: store}
}

func main() {
	mux := http.NewServeMux()
	server := newServer()
	mux.Handle("/argonauts/", wrappingMiddleware(http.HandlerFunc(server.argonautsHandler)))
	mux.HandleFunc("/movie", movieHandler)
	mux.Handle("/random", passingMiddleware(http.HandlerFunc(server.randomHandler)))
	mux.Handle("/clean", passingMiddleware(http.HandlerFunc(server.cleanHandler)))
	mux.Handle("/admin", adminHandler())
	mux.HandleFunc("/", server.notFound)
	print("running...\n")
	authMux := authMiddleware(mux)
	err := http.ListenAndServe(":8080", authMux)
	if err != nil {
		print("oh no - Medea got us")
		panic(err)
	}
}

func authMiddleware(next http.Handler) (adapter http.Handler) {
	adapter = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authEnable := os.Getenv("AUTH")
		if authEnable != "true" {
			next.ServeHTTP(w, r)
			return
		}
		token := r.Header.Get("Token")
		if token != "1234" {
			w.WriteHeader(http.StatusUnauthorized)
			w.Header().Set("Content-Type", "text/html; charset=utf-8")
			fmt.Fprintf(w, "ERROR: you are not auth...")
			return
		}
		fmt.Println("you are auth...")
		next.ServeHTTP(w, r)
	})
	return
}

func wrappingMiddleware(next http.Handler) http.Handler {
	// routing before logging to avoid logging bad requests
	next = routingMiddleware(next)
	next = loggingMiddleware(next)
	return next
}

func loggingMiddleware(next http.Handler) (adapter http.Handler) {
	adapter = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		reqTime := start.Format(time.RFC3339)
		next.ServeHTTP(w, r)
		fmt.Printf("%v\t%s\t%s\t%s\n", reqTime, r.Method, r.RequestURI, time.Since(start))
	})
	return
}

func routingMiddleware(next http.Handler) (adapter http.Handler) {
	handle := func(w http.ResponseWriter, r *http.Request) {
		pathTrim := strings.Trim(r.URL.Path, "/")
		paths := strings.Split(pathTrim, "/")
		if paths[0] != "argonauts" {
			routeErr(w)
			return
		}
		if len(paths) > 2 {
			routeErr(w)
			return
		}
		next.ServeHTTP(w, r)
	}
	adapter = http.HandlerFunc(handle)
	return
}

type passingData map[string]string

func passingMiddleware(next http.Handler) (adapter http.Handler) {
	handle := func(w http.ResponseWriter, r *http.Request) {
		// fmt.Println(r.Method)
		// fmt.Println(r.URL)
		// fmt.Println(r.Proto)
		// fmt.Println(r.Header)
		// fmt.Println(r.Header["Authorization"])
		// fmt.Println(r.Body)
		// fmt.Println(r.GetBody)
		// fmt.Println(r.ContentLength)
		// fmt.Println(r.TransferEncoding)
		// fmt.Println(r.Form)
		// fmt.Println(r.PostForm)
		// fmt.Println(r.PostFormValue())
		// fmt.Println(r.MultipartForm)
		// fmt.Println(r.Trailer)
		// fmt.Println(r.RemoteAddr)
		// fmt.Println(r.RequestURI)
		// fmt.Println(r.TLS)
		// fmt.Println(r.Response)
		var data = make(passingData)
		data["frommiddleware"] = "infosfrommiddleware"
		data["useragent"] = r.Header["User-Agent"][0]
		//lint:ignore SA1029 "data" should be his own type - but it is a mess for no gain and very limited scope
		ctx := context.WithValue(r.Context(), "data", data)
		next.ServeHTTP(w, r.WithContext(ctx))
	}
	adapter = http.HandlerFunc(handle)
	return
}

func (s *Server) randomHandler(w http.ResponseWriter, r *http.Request) {
	var useragent, frommiddleware string
	w.WriteHeader(http.StatusAccepted)
	dataRaw := r.Context().Value("data")
	if dataRaw != nil {
		data := dataRaw.(passingData)
		useragent = data["useragent"]
		frommiddleware = data["frommiddleware"]
	}
	w.Header().Set("Content-Type", "text/plain; charset=utf-8")
	fmt.Fprintf(w, "random page - %#v - %#v", useragent, frommiddleware)
}

func (s *Server) cleanHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusAccepted)
	w.Header().Set("Content-Type", "text/plain; charset=utf-8")
	fmt.Fprintf(w, "clean handler -ish")
}

func adminHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusAccepted)
		fmt.Fprintf(w, "this is admin")
	})
}

func movieHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet || r.URL.Path != "/movie" {
		routeErr(w)
		return
	}
	w.WriteHeader(http.StatusAccepted)
	w.Header().Set("Content-Type", "application/json")

	movieResp := movieS{}
	err := json.Unmarshal([]byte(movie), &movieResp)
	if err != nil {
		fmt.Fprintf(w, "error encoding %#v", err)
	}
	err = json.NewEncoder(w).Encode(movieResp)
	if err != nil {
		fmt.Fprintf(w, "error encoding %#v", err)
	}

}

func (s *Server) argonautsHandler(w http.ResponseWriter, r *http.Request) {
	pathTrim := strings.Trim(r.URL.Path, "/")
	paths := strings.Split(pathTrim, "/")
	switch r.Method {
	case http.MethodGet:
		if len(paths) == 2 {
			s.routeGetOne(w, paths[1])
			return
		}
		s.routeGetAll(w)
		return
	case http.MethodPatch, http.MethodPut:
		if len(paths) == 2 {
			s.routeUpdate(w, r, paths[1])
			return
		}
		routeErr(w)
		return
	case http.MethodDelete:
		if len(paths) == 2 {
			s.routeDelete(w, paths[1])
			return
		}
		routeErr(w)
		return
	case http.MethodPost:
		if len(paths) == 1 {
			s.routePost(w, r)
			return
		}
		routeErr(w)
		return
	default:
		routeErr(w)
	}
}

func (s *Server) routeDelete(w http.ResponseWriter, id string) {
	idn, err := strconv.Atoi(id)
	if err != nil {
		w.WriteHeader(http.StatusNotAcceptable)
		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		fmt.Fprintf(w, "ERROR: params provided is not an number")
		return
	}

	err = s.argonautsStore.delete(idn)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		fmt.Fprintf(w, "ERROR: missing id of argonaut %#v", err)
		return
	}

	w.WriteHeader(http.StatusResetContent)
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	fmt.Fprintf(w, "SUCCESS: argonaut with id %d, is out of the boat", idn)

}

func (s *Server) routePost(w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Content-Type") != "application/json" {
		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		w.WriteHeader(http.StatusNoContent)
		fmt.Fprintf(w, "ERROR: bad content type")
		return
	}

	r.Body = http.MaxBytesReader(w, r.Body, 1048576)

	var bodyReq inputs
	decoder := json.NewDecoder(r.Body)
	defer r.Body.Close()

	decoder.DisallowUnknownFields()
	err := decoder.Decode(&bodyReq)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		fmt.Fprintf(w, "ERROR: bad formated body %#v", err)
		return
	}

	CrewMember := s.argonautsStore.create(bodyReq.Name, bodyReq.Link)
	w.WriteHeader(http.StatusCreated)
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(CrewMember)
	if err != nil {
		fmt.Fprintf(w, "error encoding %#v", err)
	}

}

func (s *Server) routeUpdate(w http.ResponseWriter, r *http.Request, id string) {
	idn, err := strconv.Atoi(id)
	if err != nil {
		w.WriteHeader(http.StatusNotAcceptable)
		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		fmt.Fprintf(w, "ERROR: params provided is not an number")
		return
	}

	if r.Header.Get("Content-Type") != "application/json" {
		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		w.WriteHeader(http.StatusNoContent)
		fmt.Fprintf(w, "ERROR: bad content type")
		return
	}

	r.Body = http.MaxBytesReader(w, r.Body, 1048576)

	var bodyReq inputs
	decoder := json.NewDecoder(r.Body)
	defer r.Body.Close()

	decoder.DisallowUnknownFields()
	err = decoder.Decode(&bodyReq)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		fmt.Fprintf(w, "ERROR: bad formated body %#v", err)
		return
	}
	_, err = s.argonautsStore.update(idn, bodyReq.Name, bodyReq.Link)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		fmt.Fprintf(w, "ERROR: missing id of argonaut %#v", err)
		return
	}
	CrewMember, err := s.argonautsStore.getOne(idn)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		fmt.Fprintf(w, "ERROR: missing id of argonaut %#v", err)
		return
	}
	w.WriteHeader(http.StatusAccepted)
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(CrewMember)
	if err != nil {
		fmt.Fprintf(w, "error encoding %#v", err)
	}

}

func (s *Server) notFound(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNotFound)
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	fmt.Fprintf(w, "ERROR: not Found")
}

func routeErr(w http.ResponseWriter) {
	http.Error(w, "ERROR: bad routing", http.StatusMethodNotAllowed)
}

func (s *Server) routeGetAll(w http.ResponseWriter) {
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	err := json.NewEncoder(w).Encode(s.argonautsStore.getAll())
	if err != nil {
		fmt.Fprintf(w, "error encoding %#v", err)
	}

}

func (s *Server) routeGetOne(w http.ResponseWriter, id string) {
	idn, err := strconv.Atoi(id)
	if err != nil {
		w.WriteHeader(http.StatusNotAcceptable)
		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		fmt.Fprintf(w, "ERROR: params provided is not an number")
		return
	}
	CrewMember, err := s.argonautsStore.getOne(idn)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		fmt.Fprintf(w, "ERROR: missing id of argonaut %#v", err)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(CrewMember)
	if err != nil {
		fmt.Fprintf(w, "error encoding %#v", err)
	}

}
