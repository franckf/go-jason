package main

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
)

func TestIntegrationGetAllArgonauts(t *testing.T) {
	request, _ := http.NewRequest(http.MethodGet, "/argonauts", nil)
	response := httptest.NewRecorder()
	server := newServer()
	server.argonautsHandler(response, request)

	t.Run("Return OK http code", func(t *testing.T) {
		gotCode := response.Result().StatusCode
		if gotCode != 200 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
	t.Run("Return json content type", func(t *testing.T) {
		gotType := response.Header().Get("Content-Type")
		if gotType != "application/json" {
			t.Errorf("Bad type, got content-type: %v", gotType)
		}
	})
	t.Run("Return the original number of argonauts", func(t *testing.T) {
		gotJSON := []argonaut{}
		json.Unmarshal(response.Body.Bytes(), &gotJSON)
		gotNum := len(gotJSON)
		wantNum := 89
		if gotNum != wantNum {
			t.Errorf("Not the right number of argonauts, got %d, want %d", gotNum, wantNum)
		}
	})
	t.Run("Return correct argonaut", func(t *testing.T) {
		gotJSON := []argonaut{}
		json.Unmarshal(response.Body.Bytes(), &gotJSON)
		wantjson := []argonaut{
			{
				ID:   1,
				Name: "Acaste",
				Link: "https://fr.wikipedia.org/wiki/Acaste",
			}, {
				ID:   89,
				Name: "Zétès",
				Link: "https://fr.wikipedia.org/wiki/Bor%C3%A9ades",
			},
		}
		if !reflect.DeepEqual(gotJSON[0], wantjson[0]) || !reflect.DeepEqual(gotJSON[88], wantjson[1]) {
			t.Errorf("Bad datas, got %#v %#v, want %#v %#v", gotJSON[0], gotJSON[88], wantjson[0], wantjson[1])
		}

	})
}

func TestIntegrationErrorsGetAllWrongMethod(t *testing.T) {
	request, _ := http.NewRequest(http.MethodTrace, "/argonauts", nil)
	response := httptest.NewRecorder()
	server := newServer()
	server.argonautsHandler(response, request)

	t.Run("Return 405 http code", func(t *testing.T) {
		gotCode := response.Result().StatusCode
		if gotCode != 405 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
	t.Run("Return json content type", func(t *testing.T) {
		gotType := response.Header().Get("Content-Type")
		if gotType != "text/plain; charset=utf-8" {
			t.Errorf("Bad type, got content-type: %v", gotType)
		}
	})
	t.Run("Return error message", func(t *testing.T) {
		got := response.Body.String()
		if got != "ERROR: bad routing\n" {
			t.Errorf("Bad message, got: %v", got)
		}
	})
}

func TestIntegrationGetOneArgonaut(t *testing.T) {
	request, _ := http.NewRequest(http.MethodGet, "/argonauts/1", nil)
	response := httptest.NewRecorder()
	server := newServer()
	server.argonautsHandler(response, request)

	t.Run("Return OK http code", func(t *testing.T) {
		gotCode := response.Result().StatusCode
		if gotCode != 200 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
	t.Run("Return json content type", func(t *testing.T) {
		gotType := response.Header().Get("Content-Type")
		if gotType != "application/json" {
			t.Errorf("Bad type, got content-type: %v", gotType)
		}
	})
	t.Run("Return the correct infos", func(t *testing.T) {
		gotJSON := argonaut{}
		json.Unmarshal(response.Body.Bytes(), &gotJSON)
		wantjson := argonaut{
			ID:   1,
			Name: "Acaste",
			Link: "https://fr.wikipedia.org/wiki/Acaste",
		}
		if !reflect.DeepEqual(gotJSON, wantjson) {
			t.Errorf("Bad datas, got %#v, want %#v", gotJSON, wantjson)
		}

	})
}

func TestIntegrationGetOneArgonautBis(t *testing.T) {
	request, _ := http.NewRequest(http.MethodGet, "/argonauts/19", nil)
	response := httptest.NewRecorder()
	server := newServer()
	server.argonautsHandler(response, request)

	t.Run("Return OK http code", func(t *testing.T) {
		gotCode := response.Result().StatusCode
		if gotCode != 200 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
	t.Run("Return json content type", func(t *testing.T) {
		gotType := response.Header().Get("Content-Type")
		if gotType != "application/json" {
			t.Errorf("Bad type, got content-type: %v", gotType)
		}
	})
	t.Run("Return the correct infos", func(t *testing.T) {
		gotJSON := argonaut{}
		json.Unmarshal(response.Body.Bytes(), &gotJSON)
		wantjson := argonaut{
			ID:   19,
			Name: "Autolycos",
			Link: "https://fr.wikipedia.org/wiki/Autolycos_(mythologie)",
		}
		if !reflect.DeepEqual(gotJSON, wantjson) {
			t.Errorf("Bad datas, got %#v, want %#v", gotJSON, wantjson)
		}

	})
}

func TestIntegrationNotFound(t *testing.T) {
	request, _ := http.NewRequest(http.MethodGet, "/something", nil)
	response := httptest.NewRecorder()
	server := newServer()
	server.notFound(response, request)

	t.Run("Return http code", func(t *testing.T) {
		gotCode := response.Result().StatusCode
		if gotCode != 404 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
	t.Run("Return error message", func(t *testing.T) {
		got := response.Body.String()
		if got != "ERROR: not Found" {
			t.Errorf("Bad message, got %v", got)
		}
	})
}

func TestIntegrationGetOneArgonautErrorMissing(t *testing.T) {
	request, _ := http.NewRequest(http.MethodGet, "/argonauts/199", nil)
	response := httptest.NewRecorder()
	server := newServer()
	server.argonautsHandler(response, request)

	t.Run("Return http code", func(t *testing.T) {
		gotCode := response.Result().StatusCode
		if gotCode != 404 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
	t.Run("Return text content type", func(t *testing.T) {
		gotType := response.Header().Get("Content-Type")
		if gotType != "text/html; charset=utf-8" {
			t.Errorf("Bad type, got content-type: %v", gotType)
		}
	})
	t.Run("Return error message", func(t *testing.T) {
		got := response.Body.String()
		if got != `ERROR: missing id of argonaut &errors.errorString{s:"invalid id"}` {
			t.Errorf("Bad message, got %v", got)
		}
	})
}
func TestIntegrationGetOneArgonautErrorNotANum(t *testing.T) {
	request, _ := http.NewRequest(http.MethodGet, "/argonauts/abc", nil)
	response := httptest.NewRecorder()
	server := newServer()
	server.argonautsHandler(response, request)

	t.Run("Return http code", func(t *testing.T) {
		gotCode := response.Result().StatusCode
		if gotCode != 406 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
	t.Run("Return text content type", func(t *testing.T) {
		gotType := response.Header().Get("Content-Type")
		if gotType != "text/html; charset=utf-8" {
			t.Errorf("Bad type, got content-type: %v", gotType)
		}
	})
	t.Run("Return error message", func(t *testing.T) {
		got := response.Body.String()
		if got != "ERROR: params provided is not an number" {
			t.Errorf("Bad message, got %v", got)
		}
	})
}
func TestIntegrationGetOneArgonautErrorTooMuchParams(t *testing.T) {
	request, _ := http.NewRequest(http.MethodGet, "/argonauts/3/4/5/6", nil)
	response := httptest.NewRecorder()
	server := newServer()
	argoHandler := http.HandlerFunc(server.argonautsHandler)
	route := routingMiddleware(argoHandler)
	route.ServeHTTP(response, request)

	t.Run("Return http code", func(t *testing.T) {
		gotCode := response.Result().StatusCode
		if gotCode != 405 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
	t.Run("Return text content type", func(t *testing.T) {
		gotType := response.Header().Get("Content-Type")
		if gotType != "text/plain; charset=utf-8" {
			t.Errorf("Bad type, got content-type: %v", gotType)
		}
	})
	t.Run("Return error message", func(t *testing.T) {
		got := response.Body.String()
		if got != "ERROR: bad routing\n" {
			t.Errorf("Bad message, got %v", got)
		}
	})
}

func TestIntegrationGetOneArgonautErrorTypo(t *testing.T) {
	request, _ := http.NewRequest(http.MethodGet, "/argonaut/1", nil)
	response := httptest.NewRecorder()
	server := newServer()
	argoHandler := http.HandlerFunc(server.argonautsHandler)
	route := routingMiddleware(argoHandler)
	route.ServeHTTP(response, request)

	t.Run("Return http code", func(t *testing.T) {
		gotCode := response.Result().StatusCode
		if gotCode != 405 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
	t.Run("Return text content type", func(t *testing.T) {
		gotType := response.Header().Get("Content-Type")
		if gotType != "text/plain; charset=utf-8" {
			t.Errorf("Bad type, got content-type: %v", gotType)
		}
	})
	t.Run("Return error message", func(t *testing.T) {
		got := response.Body.String()
		if got != "ERROR: bad routing\n" {
			t.Errorf("Bad message, got %v", got)
		}
	})
}

func TestIntegrationGetAllArgonautsEndRouting(t *testing.T) {
	request, _ := http.NewRequest(http.MethodGet, "/argonauts/", nil)
	response := httptest.NewRecorder()
	server := newServer()
	server.argonautsHandler(response, request)

	t.Run("Return OK http code", func(t *testing.T) {
		gotCode := response.Result().StatusCode
		if gotCode != 200 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
	t.Run("Return json content type", func(t *testing.T) {
		gotType := response.Header().Get("Content-Type")
		if gotType != "application/json" {
			t.Errorf("Bad type, got content-type: %v", gotType)
		}
	})
	t.Run("Return the original number of argonauts", func(t *testing.T) {
		gotJSON := []argonaut{}
		json.Unmarshal(response.Body.Bytes(), &gotJSON)
		gotNum := len(gotJSON)
		wantNum := 89
		if gotNum != wantNum {
			t.Errorf("Not the right number of argonauts, got %d, want %d", gotNum, wantNum)
		}
	})
}

func TestIntegrationGetOneArgonautEndRouting(t *testing.T) {
	request, _ := http.NewRequest(http.MethodGet, "/argonauts/1/", nil)
	response := httptest.NewRecorder()
	server := newServer()
	server.argonautsHandler(response, request)

	t.Run("Return OK http code", func(t *testing.T) {
		gotCode := response.Result().StatusCode
		if gotCode != 200 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
	t.Run("Return json content type", func(t *testing.T) {
		gotType := response.Header().Get("Content-Type")
		if gotType != "application/json" {
			t.Errorf("Bad type, got content-type: %v", gotType)
		}
	})
}
