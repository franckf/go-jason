package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
)

func TestIntUpdateArgonautPatch(t *testing.T) {
	newData := `{"name":"Hercule","link":"https://fr.wikipedia.org/wiki/Hercule"}`
	bodyReq := bytes.NewBuffer([]byte(newData))
	request, _ := http.NewRequest(http.MethodPatch, "/argonauts/41", bodyReq)
	request.Header.Set("Content-Type", "application/json")
	response := httptest.NewRecorder()
	server := newServer()
	server.argonautsHandler(response, request)

	t.Run("Return OK http code", func(t *testing.T) {
		gotCode := response.Result().StatusCode
		if gotCode != 202 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
	t.Run("Return json content type", func(t *testing.T) {
		gotType := response.Header().Get("Content-Type")
		if gotType != "application/json" {
			t.Errorf("Bad type, got content-type: %v", gotType)
		}
	})
	t.Run("Return correct argonaut", func(t *testing.T) {
		gotJSON := argonaut{}
		json.Unmarshal(response.Body.Bytes(), &gotJSON)
		wantjson := argonaut{
			ID:   41,
			Name: "Hercule",
			Link: "https://fr.wikipedia.org/wiki/Hercule",
		}
		if !reflect.DeepEqual(gotJSON, wantjson) {
			t.Errorf("Bad datas, got %#v, want %#v", gotJSON, wantjson)
		}
	})
}

func TestIntUpdateArgonautPut(t *testing.T) {
	newData := `{"name":"Hercule Disney","link":"https://fr.wikipedia.org/wiki/Hercule_(Disney)"}`
	bodyReq := bytes.NewBuffer([]byte(newData))
	request, _ := http.NewRequest(http.MethodPut, "/argonauts/41", bodyReq)
	request.Header.Set("Content-Type", "application/json")
	response := httptest.NewRecorder()
	server := newServer()
	server.argonautsHandler(response, request)

	t.Run("Return OK http code", func(t *testing.T) {
		gotCode := response.Result().StatusCode
		if gotCode != 202 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
	t.Run("Return json content type", func(t *testing.T) {
		gotType := response.Header().Get("Content-Type")
		if gotType != "application/json" {
			t.Errorf("Bad type, got content-type: %v", gotType)
		}
	})
	t.Run("Return correct argonaut", func(t *testing.T) {
		gotJSON := argonaut{}
		json.Unmarshal(response.Body.Bytes(), &gotJSON)
		wantjson := argonaut{
			ID:   41,
			Name: "Hercule Disney",
			Link: "https://fr.wikipedia.org/wiki/Hercule_(Disney)",
		}
		if !reflect.DeepEqual(gotJSON, wantjson) {
			t.Errorf("Bad datas, got %#v, want %#v", gotJSON, wantjson)
		}
	})
}

func TestUpdateArgonautErrorMissing(t *testing.T) {
	newData := `{"name":"Hercule Disney","link":"https://fr.wikipedia.org/wiki/Hercule_(Disney)"}`
	bodyReq := bytes.NewBuffer([]byte(newData))
	request, _ := http.NewRequest(http.MethodPut, "/argonauts/199", bodyReq)
	request.Header.Set("Content-Type", "application/json")
	response := httptest.NewRecorder()
	server := newServer()
	server.argonautsHandler(response, request)

	t.Run("Return http code", func(t *testing.T) {
		gotCode := response.Result().StatusCode
		if gotCode != 404 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
	t.Run("Return text content type", func(t *testing.T) {
		gotType := response.Header().Get("Content-Type")
		if gotType != "text/html; charset=utf-8" {
			t.Errorf("Bad type, got content-type: %v", gotType)
		}
	})
	t.Run("Return error message", func(t *testing.T) {
		got := response.Body.String()
		if got != `ERROR: missing id of argonaut &errors.errorString{s:"invalid id"}` {
			t.Errorf("Bad message, got %v", got)
		}
	})
}
func TestUpdateArgonautErrorNotANum(t *testing.T) {
	request, _ := http.NewRequest(http.MethodPatch, "/argonauts/abc", nil)
	response := httptest.NewRecorder()
	server := newServer()
	server.argonautsHandler(response, request)

	t.Run("Return http code", func(t *testing.T) {
		gotCode := response.Result().StatusCode
		if gotCode != 406 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
	t.Run("Return text content type", func(t *testing.T) {
		gotType := response.Header().Get("Content-Type")
		if gotType != "text/html; charset=utf-8" {
			t.Errorf("Bad type, got content-type: %v", gotType)
		}
	})
	t.Run("Return error message", func(t *testing.T) {
		got := response.Body.String()
		if got != "ERROR: params provided is not an number" {
			t.Errorf("Bad message, got %v", got)
		}
	})
}
func TestUpdateArgonautErrorTooMuchParams(t *testing.T) {
	request, _ := http.NewRequest(http.MethodPut, "/argonauts/3/4/5/6", nil)
	response := httptest.NewRecorder()
	server := newServer()
	server.argonautsHandler(response, request)

	t.Run("Return http code", func(t *testing.T) {
		gotCode := response.Result().StatusCode
		if gotCode != 405 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
	t.Run("Return text content type", func(t *testing.T) {
		gotType := response.Header().Get("Content-Type")
		if gotType != "text/plain; charset=utf-8" {
			t.Errorf("Bad type, got content-type: %v", gotType)
		}
	})
	t.Run("Return error message", func(t *testing.T) {
		got := response.Body.String()
		if got != "ERROR: bad routing\n" {
			t.Errorf("Bad message, got %v", got)
		}
	})
}

func TestUpdateArgonautErrorTypo(t *testing.T) {
	request, _ := http.NewRequest(http.MethodPatch, "/argonaut/1", nil)
	response := httptest.NewRecorder()
	server := newServer()
	argoHandler := http.HandlerFunc(server.argonautsHandler)
	route := routingMiddleware(argoHandler)
	route.ServeHTTP(response, request)

	t.Run("Return http code", func(t *testing.T) {
		gotCode := response.Result().StatusCode
		if gotCode != 405 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
	t.Run("Return text content type", func(t *testing.T) {
		gotType := response.Header().Get("Content-Type")
		if gotType != "text/plain; charset=utf-8" {
			t.Errorf("Bad type, got content-type: %v", gotType)
		}
	})
	t.Run("Return error message", func(t *testing.T) {
		got := response.Body.String()
		if got != "ERROR: bad routing\n" {
			t.Errorf("Bad message, got %v", got)
		}
	})
}

func TestUpdateArgonautErrorContentType(t *testing.T) {
	newData := `{"name":"Hercule Disney","link":"https://fr.wikipedia.org/wiki/Hercule_(Disney)"}`
	bodyReq := bytes.NewBuffer([]byte(newData))
	request, _ := http.NewRequest(http.MethodPut, "/argonauts/41", bodyReq)
	request.Header.Set("Content-Type", "pplication/json")
	response := httptest.NewRecorder()
	server := newServer()
	server.argonautsHandler(response, request)

	t.Run("Return http code", func(t *testing.T) {
		gotCode := response.Result().StatusCode
		if gotCode != 204 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
	t.Run("Return text content type", func(t *testing.T) {
		gotType := response.Header().Get("Content-Type")
		if gotType != "text/html; charset=utf-8" {
			t.Errorf("Bad type, got content-type: %v", gotType)
		}
	})
}

func TestUpdateArgonautErrorBadjson(t *testing.T) {
	newData := `{"name":"Hercule Disney","link":"https://fr.wikipedia.org/wiki/Hercule_(Disney)"`
	bodyReq := bytes.NewBuffer([]byte(newData))
	request, _ := http.NewRequest(http.MethodPut, "/argonauts/41", bodyReq)
	request.Header.Set("Content-Type", "application/json")
	response := httptest.NewRecorder()
	server := newServer()
	server.argonautsHandler(response, request)

	t.Run("Return http code", func(t *testing.T) {
		gotCode := response.Result().StatusCode
		if gotCode != 400 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
	t.Run("Return text content type", func(t *testing.T) {
		gotType := response.Header().Get("Content-Type")
		if gotType != "text/html; charset=utf-8" {
			t.Errorf("Bad type, got content-type: %v", gotType)
		}
	})
	t.Run("Return error message", func(t *testing.T) {
		got := response.Body.String()
		if got != "ERROR: bad formated body &errors.errorString{s:\"unexpected EOF\"}" {
			t.Errorf("Bad message, got %v", got)
		}
	})
}
