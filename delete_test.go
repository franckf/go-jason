package main

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestIntDeleteArgonaut(t *testing.T) {
	request, _ := http.NewRequest(http.MethodDelete, "/argonauts/86", nil)
	response := httptest.NewRecorder()
	server := newServer()
	server.argonautsHandler(response, request)

	t.Run("Return OK http code", func(t *testing.T) {
		gotCode := response.Result().StatusCode
		if gotCode != 205 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
	t.Run("Return text content type", func(t *testing.T) {
		gotType := response.Header().Get("Content-Type")
		if gotType != "text/html; charset=utf-8" {
			t.Errorf("Bad type, got content-type: %v", gotType)
		}
	})
	t.Run("Return error message", func(t *testing.T) {
		got := response.Body.String()
		if got != "SUCCESS: argonaut with id 86, is out of the boat" {
			t.Errorf("Bad message, got %v", got)
		}
	})

	requestGet, _ := http.NewRequest(http.MethodGet, "/argonauts", nil)
	responseGet := httptest.NewRecorder()
	server.argonautsHandler(responseGet, requestGet)
	t.Run("Return the new number of argonauts", func(t *testing.T) {
		gotJSON := []argonaut{}
		json.Unmarshal(responseGet.Body.Bytes(), &gotJSON)
		gotNum := len(gotJSON)
		wantNum := 88
		if gotNum != wantNum {
			t.Errorf("Not the right number of argonauts, got %d, want %d", gotNum, wantNum)
		}
	})

	requestGet86, _ := http.NewRequest(http.MethodGet, "/argonauts/86", nil)
	responseGet86 := httptest.NewRecorder()
	server.argonautsHandler(responseGet86, requestGet86)
	t.Run("Return http code", func(t *testing.T) {
		gotCode := responseGet86.Result().StatusCode
		if gotCode != 404 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
	t.Run("Return text content type", func(t *testing.T) {
		gotType := responseGet86.Header().Get("Content-Type")
		if gotType != "text/html; charset=utf-8" {
			t.Errorf("Bad type, got content-type: %v", gotType)
		}
	})
	t.Run("Return error message", func(t *testing.T) {
		got := responseGet86.Body.String()
		if got != `ERROR: missing id of argonaut &errors.errorString{s:"invalid id"}` {
			t.Errorf("Bad message, got %v", got)
		}
	})
}

func TestIntDeleteArgonautErrorMissing(t *testing.T) {
	request, _ := http.NewRequest(http.MethodDelete, "/argonauts/199", nil)
	response := httptest.NewRecorder()
	server := newServer()
	server.argonautsHandler(response, request)

	t.Run("Return http code", func(t *testing.T) {
		gotCode := response.Result().StatusCode
		if gotCode != 404 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
	t.Run("Return text content type", func(t *testing.T) {
		gotType := response.Header().Get("Content-Type")
		if gotType != "text/html; charset=utf-8" {
			t.Errorf("Bad type, got content-type: %v", gotType)
		}
	})
	t.Run("Return error message", func(t *testing.T) {
		got := response.Body.String()
		if got != `ERROR: missing id of argonaut &errors.errorString{s:"invalid id"}` {
			t.Errorf("Bad message, got %v", got)
		}
	})
}
func TestIntDeleteArgonautErrorNotANum(t *testing.T) {
	request, _ := http.NewRequest(http.MethodDelete, "/argonauts/abc", nil)
	response := httptest.NewRecorder()
	server := newServer()
	server.argonautsHandler(response, request)

	t.Run("Return http code", func(t *testing.T) {
		gotCode := response.Result().StatusCode
		if gotCode != 406 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
	t.Run("Return text content type", func(t *testing.T) {
		gotType := response.Header().Get("Content-Type")
		if gotType != "text/html; charset=utf-8" {
			t.Errorf("Bad type, got content-type: %v", gotType)
		}
	})
	t.Run("Return error message", func(t *testing.T) {
		got := response.Body.String()
		if got != "ERROR: params provided is not an number" {
			t.Errorf("Bad message, got %v", got)
		}
	})
}
func TestIntDeleteArgonautErrorTooMuchParams(t *testing.T) {
	request, _ := http.NewRequest(http.MethodDelete, "/argonauts/3/4/5/6", nil)
	response := httptest.NewRecorder()
	server := newServer()
	server.argonautsHandler(response, request)

	t.Run("Return http code", func(t *testing.T) {
		gotCode := response.Result().StatusCode
		if gotCode != 405 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
	t.Run("Return text content type", func(t *testing.T) {
		gotType := response.Header().Get("Content-Type")
		if gotType != "text/plain; charset=utf-8" {
			t.Errorf("Bad type, got content-type: %v", gotType)
		}
	})
	t.Run("Return error message", func(t *testing.T) {
		got := response.Body.String()
		if got != "ERROR: bad routing\n" {
			t.Errorf("Bad message, got %v", got)
		}
	})
}

func TestIntDeleteArgonautErrorTypo(t *testing.T) {
	request, _ := http.NewRequest(http.MethodPatch, "/argonaut/1", nil)
	response := httptest.NewRecorder()
	server := newServer()
	argoHandler := http.HandlerFunc(server.argonautsHandler)
	route := routingMiddleware(argoHandler)
	route.ServeHTTP(response, request)

	t.Run("Return http code", func(t *testing.T) {
		gotCode := response.Result().StatusCode
		if gotCode != 405 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
	t.Run("Return text content type", func(t *testing.T) {
		gotType := response.Header().Get("Content-Type")
		if gotType != "text/plain; charset=utf-8" {
			t.Errorf("Bad type, got content-type: %v", gotType)
		}
	})
	t.Run("Return error message", func(t *testing.T) {
		got := response.Body.String()
		if got != "ERROR: bad routing\n" {
			t.Errorf("Bad message, got %v", got)
		}
	})
}
