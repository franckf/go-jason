package main

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestMovieResponse(t *testing.T) {
	t.Parallel()
	request := httptest.NewRequest(http.MethodGet, "/movie", nil)
	response := httptest.NewRecorder()
	movieHandler(response, request)

	t.Run("OK response code", func(t *testing.T) {
		got := response.Result().StatusCode
		want := 202
		if got != want {
			t.Error("got ", got, " want ", want)
		}
	})
	t.Run("JSON content type", func(t *testing.T) {
		got := response.Header().Get("Content-Type")
		want := "application/json"
		if got != want {
			t.Error("Bad content type, got ", got)
		}
	})
	t.Run("Response not empty", func(t *testing.T) {
		got := response.Body.String()
		if len(got) == 0 {
			t.Error("Response is empty")
		}
	})
}

func TestMovieContent(t *testing.T) {
	t.Parallel()
	request := httptest.NewRequest(http.MethodGet, "/movie", nil)
	response := httptest.NewRecorder()
	movieHandler(response, request)
	t.Run("returns movies values", func(t *testing.T) {
		got := movieS{}
		json.Unmarshal(response.Body.Bytes(), &got)
		if got.Title != "Jason and the Argonauts" {
			t.Error("Bad title", got.Title)
		}
		if got.Countries[0] != "United States" {
			t.Error("Bad country", got.Countries[0])
		}
		if got.ReleaseYear != 1963 {
			t.Error("Bad year", got.ReleaseYear)
		}
	})

}

func TestMovieResponseMethod(t *testing.T) {
	t.Parallel()
	request := httptest.NewRequest(http.MethodPost, "/movie", nil)
	response := httptest.NewRecorder()
	movieHandler(response, request)

	t.Run("response code", func(t *testing.T) {
		got := response.Result().StatusCode
		want := 405
		if got != want {
			t.Error("got ", got, " want ", want)
		}
	})
	t.Run("content type", func(t *testing.T) {
		got := response.Header().Get("Content-Type")
		want := "text/plain; charset=utf-8"
		if got != want {
			t.Error("Bad content type, got ", got)
		}
	})
	t.Run("return error message", func(t *testing.T) {
		got := response.Body.String()
		if got != "ERROR: bad routing\n" {
			t.Errorf("Bad message, got: %v", got)
		}
	})
}

func TestMoviesResponse(t *testing.T) {
	t.Parallel()
	request := httptest.NewRequest(http.MethodGet, "/movies", nil)
	response := httptest.NewRecorder()
	movieHandler(response, request)

	t.Run("response code", func(t *testing.T) {
		got := response.Result().StatusCode
		want := 405
		if got != want {
			t.Error("got ", got, " want ", want)
		}
	})
	t.Run("content type", func(t *testing.T) {
		got := response.Header().Get("Content-Type")
		want := "text/plain; charset=utf-8"
		if got != want {
			t.Error("Bad content type, got ", got)
		}
	})
	t.Run("return error message", func(t *testing.T) {
		got := response.Body.String()
		if got != "ERROR: bad routing\n" {
			t.Errorf("Bad message, got: %v", got)
		}
	})
}
func TestMovieResponseWithParams(t *testing.T) {
	t.Parallel()
	request := httptest.NewRequest(http.MethodGet, "/movie/1963", nil)
	response := httptest.NewRecorder()
	movieHandler(response, request)

	t.Run("response code", func(t *testing.T) {
		got := response.Result().StatusCode
		want := 405
		if got != want {
			t.Error("got ", got, " want ", want)
		}
	})
	t.Run("content type", func(t *testing.T) {
		got := response.Header().Get("Content-Type")
		want := "text/plain; charset=utf-8"
		if got != want {
			t.Error("Bad content type, got ", got)
		}
	})
	t.Run("return error message", func(t *testing.T) {
		got := response.Body.String()
		if got != "ERROR: bad routing\n" {
			t.Errorf("Bad message, got: %v", got)
		}
	})
}
