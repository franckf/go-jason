package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
)

func TestPostArgonaut(t *testing.T) {
	newData := `{"name":"Medea","link":"https://fr.wikipedia.org/wiki/Medea"}`
	bodyReq := bytes.NewBuffer([]byte(newData))
	request, _ := http.NewRequest(http.MethodPost, "/argonauts", bodyReq)
	request.Header.Set("Content-Type", "application/json")
	response := httptest.NewRecorder()
	server := newServer()
	server.argonautsHandler(response, request)

	t.Run("Return OK http code", func(t *testing.T) {
		gotCode := response.Result().StatusCode
		if gotCode != 201 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
	t.Run("Return json content type", func(t *testing.T) {
		gotType := response.Header().Get("Content-Type")
		if gotType != "application/json" {
			t.Errorf("Bad type, got content-type: %v", gotType)
		}
	})
	t.Run("Return correct argonaut", func(t *testing.T) {
		gotJSON := argonaut{}
		json.Unmarshal(response.Body.Bytes(), &gotJSON)
		wantjson := argonaut{
			ID:   90,
			Name: "Medea",
			Link: "https://fr.wikipedia.org/wiki/Medea",
		}
		if !reflect.DeepEqual(gotJSON, wantjson) {
			t.Errorf("Bad datas, got %#v, want %#v", gotJSON, wantjson)
		}
	})

	requestGet, _ := http.NewRequest(http.MethodGet, "/argonauts", nil)
	responseGet := httptest.NewRecorder()
	server.argonautsHandler(responseGet, requestGet)
	t.Run("Return the new number of argonauts", func(t *testing.T) {
		gotJSON := []argonaut{}
		json.Unmarshal(responseGet.Body.Bytes(), &gotJSON)
		gotNum := len(gotJSON)
		wantNum := 90
		if gotNum != wantNum {
			t.Errorf("Not the right number of argonauts, got %d, want %d", gotNum, wantNum)
		}
	})
}

func TestPostArgonautErrorMissing(t *testing.T) {
	newData := `{"name":"Medea","link":"https://fr.wikipedia.org/wiki/Medea"}`
	bodyReq := bytes.NewBuffer([]byte(newData))
	request, _ := http.NewRequest(http.MethodPost, "/argonauts", bodyReq)
	request.Header.Set("Content-Type", "application/json")
	response := httptest.NewRecorder()
	server := newServer()
	server.argonautsHandler(response, request)

	requestGet, _ := http.NewRequest(http.MethodGet, "/argonauts/90", nil)
	responseGet := httptest.NewRecorder()
	server.argonautsHandler(responseGet, requestGet)

	t.Run("Return OK http code", func(t *testing.T) {
		gotCode := responseGet.Result().StatusCode
		if gotCode != 200 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
	t.Run("Return json content type", func(t *testing.T) {
		gotType := responseGet.Header().Get("Content-Type")
		if gotType != "application/json" {
			t.Errorf("Bad type, got content-type: %v", gotType)
		}
	})
	t.Run("Return correct one", func(t *testing.T) {
		gotJSON := argonaut{}
		json.Unmarshal(responseGet.Body.Bytes(), &gotJSON)
		wantjson := argonaut{
			ID:   90,
			Name: "Medea",
			Link: "https://fr.wikipedia.org/wiki/Medea",
		}
		if !reflect.DeepEqual(gotJSON, wantjson) {
			t.Errorf("Bad datas, got %#v, want %#v", gotJSON, wantjson)
		}
	})
}

func TestPostArgonautErrorTooMuchParams(t *testing.T) {
	request, _ := http.NewRequest(http.MethodPost, "/argonauts/e", nil)
	response := httptest.NewRecorder()
	server := newServer()
	server.argonautsHandler(response, request)

	t.Run("Return http code", func(t *testing.T) {
		gotCode := response.Result().StatusCode
		if gotCode != 405 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
	t.Run("Return text content type", func(t *testing.T) {
		gotType := response.Header().Get("Content-Type")
		if gotType != "text/plain; charset=utf-8" {
			t.Errorf("Bad type, got content-type: %v", gotType)
		}
	})
	t.Run("Return error message", func(t *testing.T) {
		got := response.Body.String()
		if got != "ERROR: bad routing\n" {
			t.Errorf("Bad message, got %v", got)
		}
	})
}

func TestPostArgonautErrorTooMuchParamsBis(t *testing.T) {
	request, _ := http.NewRequest(http.MethodPost, "/argonauts/3", nil)
	response := httptest.NewRecorder()
	server := newServer()
	server.argonautsHandler(response, request)

	t.Run("Return http code", func(t *testing.T) {
		gotCode := response.Result().StatusCode
		if gotCode != 405 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
	t.Run("Return text content type", func(t *testing.T) {
		gotType := response.Header().Get("Content-Type")
		if gotType != "text/plain; charset=utf-8" {
			t.Errorf("Bad type, got content-type: %v", gotType)
		}
	})
	t.Run("Return error message", func(t *testing.T) {
		got := response.Body.String()
		if got != "ERROR: bad routing\n" {
			t.Errorf("Bad message, got %v", got)
		}
	})
}

func TestPostArgonautErrorTypo(t *testing.T) {
	request, _ := http.NewRequest(http.MethodPost, "/argonaut", nil)
	response := httptest.NewRecorder()
	server := newServer()
	argoHandler := http.HandlerFunc(server.argonautsHandler)
	route := routingMiddleware(argoHandler)
	route.ServeHTTP(response, request)

	t.Run("Return http code", func(t *testing.T) {
		gotCode := response.Result().StatusCode
		if gotCode != 405 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
	t.Run("Return text content type", func(t *testing.T) {
		gotType := response.Header().Get("Content-Type")
		if gotType != "text/plain; charset=utf-8" {
			t.Errorf("Bad type, got content-type: %v", gotType)
		}
	})
	t.Run("Return error message", func(t *testing.T) {
		got := response.Body.String()
		if got != "ERROR: bad routing\n" {
			t.Errorf("Bad message, got %v", got)
		}
	})
}

func TestPostArgonautErrorContentType(t *testing.T) {
	newData := `{"name":"Medea","link":"https://fr.wikipedia.org/wiki/Medea"}`
	bodyReq := bytes.NewBuffer([]byte(newData))
	request, _ := http.NewRequest(http.MethodPost, "/argonauts", bodyReq)
	request.Header.Set("Content-Type", "pplication/json")
	response := httptest.NewRecorder()
	server := newServer()
	server.argonautsHandler(response, request)

	t.Run("Return http code", func(t *testing.T) {
		gotCode := response.Result().StatusCode
		if gotCode != 204 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
	t.Run("Return text content type", func(t *testing.T) {
		gotType := response.Header().Get("Content-Type")
		if gotType != "text/html; charset=utf-8" {
			t.Errorf("Bad type, got content-type: %v", gotType)
		}
	})
}

func TestPostArgonautErrorBadjson(t *testing.T) {
	newData := `{"name":"Medea","link":"https://fr.wikipedia.org/wiki/Medea"`
	bodyReq := bytes.NewBuffer([]byte(newData))
	request, _ := http.NewRequest(http.MethodPost, "/argonauts", bodyReq)
	request.Header.Set("Content-Type", "application/json")
	response := httptest.NewRecorder()
	server := newServer()
	server.argonautsHandler(response, request)

	t.Run("Return http code", func(t *testing.T) {
		gotCode := response.Result().StatusCode
		if gotCode != 400 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
	t.Run("Return text content type", func(t *testing.T) {
		gotType := response.Header().Get("Content-Type")
		if gotType != "text/html; charset=utf-8" {
			t.Errorf("Bad type, got content-type: %v", gotType)
		}
	})
	t.Run("Return error message", func(t *testing.T) {
		got := response.Body.String()
		if got != "ERROR: bad formated body &errors.errorString{s:\"unexpected EOF\"}" {
			t.Errorf("Bad message, got %v", got)
		}
	})
}
