package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

func TestRoutingMiddleware(t *testing.T) {

	fakeHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {})
	req := httptest.NewRequest(http.MethodGet, "/argonuts", nil)
	res := httptest.NewRecorder()
	fakeHandler(res, req)

	route := routingMiddleware(fakeHandler)
	route.ServeHTTP(res, req)

	t.Run("Return http code", func(t *testing.T) {
		gotCode := res.Result().StatusCode
		if gotCode != 405 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
	t.Run("Return text content type", func(t *testing.T) {
		gotType := res.Header().Get("Content-Type")
		if gotType != "text/plain; charset=utf-8" {
			t.Errorf("Bad type, got content-type: %v", gotType)
		}
	})
	t.Run("Return error message", func(t *testing.T) {
		got := res.Body.String()
		if got != "ERROR: bad routing\n" {
			t.Errorf("Bad message, got %v", got)
		}
	})
}

func TestPassingMiddleware(t *testing.T) {

	fakeHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var useragent, frommiddleware string
		dataRaw := r.Context().Value("data")
		if dataRaw != nil {
			data := dataRaw.(passingData)
			useragent = data["useragent"]
			frommiddleware = data["frommiddleware"]
		}
		fmt.Fprintf(w, "random page - %#v - %#v", useragent, frommiddleware)
	})
	req := httptest.NewRequest(http.MethodGet, "/random", nil)
	req.Header.Set("User-Agent", "unittest")
	res := httptest.NewRecorder()

	route := passingMiddleware(fakeHandler)
	route.ServeHTTP(res, req)

	t.Run("Return info message", func(t *testing.T) {
		got := res.Body.String()
		if got != `random page - "unittest" - "infosfrommiddleware"` {
			t.Errorf("Bad message, got  %v  ", got)
		}
	})
}

//authMiddleware

func TestWrappingMiddleware(t *testing.T) {

	fakeHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {})
	req := httptest.NewRequest(http.MethodGet, "/argonuts", nil)
	res := httptest.NewRecorder()
	fakeHandler(res, req)

	route := wrappingMiddleware(fakeHandler)
	route.ServeHTTP(res, req)

	t.Run("Return http code", func(t *testing.T) {
		gotCode := res.Result().StatusCode
		if gotCode != 405 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
	t.Run("Return text content type", func(t *testing.T) {
		gotType := res.Header().Get("Content-Type")
		if gotType != "text/plain; charset=utf-8" {
			t.Errorf("Bad type, got content-type: %v", gotType)
		}
	})
	t.Run("Return error message", func(t *testing.T) {
		got := res.Body.String()
		if got != "ERROR: bad routing\n" {
			t.Errorf("Bad message, got %v", got)
		}
	})
}

func TestBadAuthMiddleware(t *testing.T) {

	os.Setenv("AUTH", "true")

	fakeHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {})
	req := httptest.NewRequest(http.MethodGet, "/argonauts", nil)
	res := httptest.NewRecorder()
	fakeHandler(res, req)

	route := authMiddleware(fakeHandler)
	route.ServeHTTP(res, req)

	t.Run("Return http code", func(t *testing.T) {
		gotCode := res.Result().StatusCode
		if gotCode != 401 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
	t.Run("Return text content type", func(t *testing.T) {
		gotType := res.Header().Get("Content-Type")
		if gotType != "text/html; charset=utf-8" {
			t.Errorf("Bad type, got content-type: %v", gotType)
		}
	})
	t.Run("Return error message", func(t *testing.T) {
		got := res.Body.String()
		if got != "ERROR: you are not auth..." {
			t.Errorf("Bad message, got %v", got)
		}
	})
}

func TestBadTokenAuthMiddleware(t *testing.T) {

	os.Setenv("AUTH", "true")

	fakeHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {})
	req := httptest.NewRequest(http.MethodGet, "/argonauts", nil)
	req.Header.Set("Token", "4321")
	res := httptest.NewRecorder()
	fakeHandler(res, req)

	route := authMiddleware(fakeHandler)
	route.ServeHTTP(res, req)

	t.Run("Return http code", func(t *testing.T) {
		gotCode := res.Result().StatusCode
		if gotCode != 401 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
	t.Run("Return text content type", func(t *testing.T) {
		gotType := res.Header().Get("Content-Type")
		if gotType != "text/html; charset=utf-8" {
			t.Errorf("Bad type, got content-type: %v", gotType)
		}
	})
	t.Run("Return error message", func(t *testing.T) {
		got := res.Body.String()
		if got != "ERROR: you are not auth..." {
			t.Errorf("Bad message, got %v", got)
		}
	})
}

func TestGoodTokenAuthMiddleware(t *testing.T) {

	os.Setenv("AUTH", "true")

	fakeHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {})
	req := httptest.NewRequest(http.MethodGet, "/argonauts", nil)
	req.Header.Set("Token", "1234")
	res := httptest.NewRecorder()
	fakeHandler(res, req)

	route := authMiddleware(fakeHandler)
	route.ServeHTTP(res, req)

	t.Run("Return http code", func(t *testing.T) {
		gotCode := res.Result().StatusCode
		if gotCode != 200 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
}
