package main

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestRoutingRandom(t *testing.T) {

	req := httptest.NewRequest(http.MethodGet, "/random", nil)
	req.Header.Set("User-Agent", "unittest")
	res := httptest.NewRecorder()

	server := newServer()
	server.randomHandler(res, req)

	t.Run("Return http code", func(t *testing.T) {
		gotCode := res.Result().StatusCode
		if gotCode != 202 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
	t.Run("Return text content type", func(t *testing.T) {
		gotType := res.Header().Get("Content-Type")
		if gotType != "text/plain; charset=utf-8" {
			t.Errorf("Bad type, got content-type: %v", gotType)
		}
	})
	t.Run("Return info message", func(t *testing.T) {
		got := res.Body.String()
		if got != `random page - "" - ""` {
			t.Errorf("Bad message, got %v", got)
		}
	})
}

func TestRoutingClean(t *testing.T) {

	req := httptest.NewRequest(http.MethodGet, "/clean", nil)
	res := httptest.NewRecorder()

	server := newServer()
	server.cleanHandler(res, req)

	t.Run("Return http code", func(t *testing.T) {
		gotCode := res.Result().StatusCode
		if gotCode != 202 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
	t.Run("Return text content type", func(t *testing.T) {
		gotType := res.Header().Get("Content-Type")
		if gotType != "text/plain; charset=utf-8" {
			t.Errorf("Bad type, got content-type: %v", gotType)
		}
	})
	t.Run("Return info message", func(t *testing.T) {
		got := res.Body.String()
		if got != `clean handler -ish` {
			t.Errorf("Bad message, got %v", got)
		}
	})
}

func TestRoutingAdmin(t *testing.T) {

	req := httptest.NewRequest(http.MethodGet, "/admin", nil)
	res := httptest.NewRecorder()

	adminHandler().ServeHTTP(res, req)

	t.Run("Return http code", func(t *testing.T) {
		gotCode := res.Result().StatusCode
		if gotCode != 202 {
			t.Errorf("Bad code response, got %d", gotCode)
		}
	})
	t.Run("Return info message", func(t *testing.T) {
		got := res.Body.String()
		if got != `this is admin` {
			t.Errorf("Bad message, got %v", got)
		}
	})
}
