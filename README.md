# go-jason
*and the Argonauts*

A go implementation of json-placeholder. Standard lib only.

\-\> spagetti code, because I have to test a lot - packaging needed  
\-\> need refactor in tests

## Install and run

`podman run --env AUTH='true' --publish 8080:8080 --interactive --rm registry.gitlab.com/franckf/go-jason:1-0-0`
